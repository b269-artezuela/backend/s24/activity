

// CUBE
let getCube = 2**3;


let cube = (number) => {
	console.log(`The cube of ${number} is ${getCube}`)
};

cube(2);



// ADDRESS
let address

address = ["258 Washington Ave NW", "California 90011"];

let [city,state] = address

console.log(`I live at ${city}, ${state}`)


// ANIMAL

let animal 

animal =  {
	name : "Lolong",
	weight : "1075 kgs",
	measurement : "20 ft 3 in.",
}

let{name,weight,measurement} = animal

console.log(`${name} was a saltwater crocodile. He weighed at ${weight} with a measurement of ${measurement} `)



// ARRAY NUMBERS
let numbers 
numbers = [1,2,3,4,5]

numbers.forEach((number) => {
	console.log(number)
})

//REDUCE 


const reduceNumber = numbers.reduce((total,number) => {
    return total+number
},0);

console.log(reduceNumber)

//COnstructor

class Dog {
	constructor (name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;

	}
}

let myDog = new Dog("Bantay",12,"Pitbull")
console.log(myDog)

